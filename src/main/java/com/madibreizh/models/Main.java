package com.madibreizh.models;

import lombok.Data;

@Data
public class Main {

    /** Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit. */
    private double temp;

    /** Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa. */
    private int pressure;

    /** Humidity, %. */
    private int humidity;
}
