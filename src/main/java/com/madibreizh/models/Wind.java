package com.madibreizh.models;

import lombok.Data;

@Data
public class Wind {

	/** Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour. */
    private double speed;

    /** Wind direction, degrees (meteorological). */
    private double deg;
}
