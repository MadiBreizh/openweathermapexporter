package com.madibreizh.models;

import lombok.Data;

/**
 * Weather parameters in API response.
 * @see https://openweathermap.org/current#current_JSON
 */
@Data
public class Weather {

    private Long dt;

    private Main main;

    private Wind wind;
}
