package com.madibreizh;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.madibreizh.models.Weather;

import io.prometheus.client.Gauge;
import io.prometheus.client.exporter.HTTPServer;
import lombok.NonNull;
import picocli.CommandLine;
import picocli.CommandLine.Option;

public class OpenWeatherMapExporter implements Runnable {

    private static final Logger LOG;

    private static final String API_URL;

    private static final HttpClient HTTP_CLIENT;

    private static final Gauge GAUGE_TEMP;
    private static final Gauge GAUGE_PRESSURE;
    private static final Gauge GAUGE_HUMIDITY;
    private static final Gauge GAUGE_WIND_SPEED;
    private static final Gauge GAUGE_WIND_DIRECTION;


    static {
        LOG = (Logger) LoggerFactory.getLogger(OpenWeatherMapExporter.class);
        HTTP_CLIENT = HttpClient.newHttpClient();
        API_URL = "http://api.openweathermap.org/data/2.5/weather?id=%s&units=metric&appid=%s";

        GAUGE_TEMP = Gauge.build().namespace("openweathermap").name("temperature_celsius").help("Temperature in °C").register();
        GAUGE_PRESSURE = Gauge.build().namespace("openweathermap").name("pressure_hpa").help("Pressure in HPA").register();
        GAUGE_HUMIDITY = Gauge.build().namespace("openweathermap").name("humidity_pourcentage").help("humidity_pourcentage").register();
        GAUGE_WIND_SPEED = Gauge.build().namespace("openweathermap").name("wind_speed_ms").help("Wind speed in ms").register();
        GAUGE_WIND_DIRECTION = Gauge.build().namespace("openweathermap").name("wind_dir_deg").help("Wind direction in deg").register();
    }


    @Option(names = {"-k", "--key"}, description = "The api key", required = true)
    private String apiKey;

    @Option(names = {"-l", "--location"}, description = "The location id", required = true)
    private String location;

    @Option(names = {"-p", "--port"}, description = "The port number")
    private Integer port = 8082;

    @Option(names = {"-d", "--delay"}, description = "Delay between each call with api")
    private Integer delay = 5;

    @Option(names = {"-v", "--verbose"}, description = "Active logs")
    private boolean verbose;

    public static void main(String[] args) {
        new CommandLine(new OpenWeatherMapExporter()).execute(args);
    }

    /**
     * Initializes request for API.
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    private HttpRequest initRequest() throws FileNotFoundException, IOException {
        final String url = String.format(API_URL, this.location, this.apiKey);

        return HttpRequest.newBuilder().uri(URI.create(url)).build();
    }

    /**
     * Fetches data from API for given {@link HttpRequest}.
     * @param request
     */
    private static Weather fetchData(final HttpRequest request) {
        Weather result = null;

        try {
            final HttpResponse<String> response = HTTP_CLIENT.send(request, HttpResponse.BodyHandlers.ofString());
            result = new Gson().fromJson(response.body(), Weather.class);
        } catch (IOException | InterruptedException e) {
            LOG.error("Cannot fetch data from api.", e);
        }

        return result;
    }

    /**
     * Updates metrics with given {@link Weather}.
     * @param weather
     */
    private static void updateData(@NonNull final Weather weather) {
        GAUGE_TEMP.set(weather.getMain().getTemp());
        GAUGE_PRESSURE.set(weather.getMain().getPressure());
        GAUGE_HUMIDITY.set(weather.getMain().getHumidity());
        GAUGE_WIND_SPEED.set(weather.getWind().getSpeed());
        GAUGE_WIND_DIRECTION.set(weather.getWind().getDeg());
        OpenWeatherMapExporter.LOG.info(weather.toString());
    }

    @Override
    public void run() {
        if (!this.verbose) {
            LOG.setLevel(Level.ERROR);
        }

        try {
            final HttpRequest request = initRequest();

            final Thread bgThread = new Thread(() -> {
                while (true) {
                    try {
                        final Weather weather = fetchData(request);
                        updateData(weather);
                        Thread.sleep(this.delay * 60 * 1000);
                    } catch (InterruptedException e) {
                        LOG.error("Thread cannot set status sleep.", e);
                        System.exit(-1);
                    }
                }
            });
            bgThread.start();
        } catch (IOException e) {
            LOG.error("Request cannot be create.", e);
            System.exit(-1);
        }


        try {
            new HTTPServer(this.port);
        } catch (IOException e) {
            LOG.error("Server exception.", e);
            System.exit(-1);
        }
    }
}
